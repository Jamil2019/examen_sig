<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communication', function (Blueprint $table) {
            $table->bigIncrements('id_communication');
            $table->string('name_account')->nullable();
            $table->string('twilio_phone')->nullable();
            $table->boolean('call_whisper')->nullable();
            $table->string('call_whisper_delay')->nullable();
            $table->boolean('voicemail')->nullable();
            $table->string('voicemail_delay')->nullable();
            $table->string('voicemail_file')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communication');
    }
}
