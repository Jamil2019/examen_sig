<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room', function (Blueprint $table) {
            $table->bigIncrements('id_room');
            $table->string('city_room')->nullable();
            $table->string('address_rom')->nullable();
            $table->string('coordenates')->nullable();
            $table->string('img_room')->nullable();
            $table->string('description')->nullable();
            $table->bigInteger('id_account')->unsigned();
            $table->bigInteger('id_state')->unsigned();
            $table->bigInteger('id_contry')->unsigned();
            $table->bigInteger('id_reputation')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room');
    }
}
