<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRoomForeingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('room', function (Blueprint $table) {
            $table->foreign('id_account')->references('id_account')->('account');
            $table->foreign('id_state')->references('id_state')->('state');
            $table->foreign('id_contry')->references('id_contry')->('contry');
            $table->foreign('id_reputation')->references('id_reputation')->('reputation');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('room', function (Blueprint $table) {
            $table->dropforeign(['id_account']);
            $table->dropforeign(['id_state']);
            $table->dropforeign(['id_contry']);
            $table->dropforeign(['id_reputation']);
            //
        });
    }
}
