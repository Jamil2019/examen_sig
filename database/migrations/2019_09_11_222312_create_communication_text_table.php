<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicationTextTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communication_text', function (Blueprint $table) {
            $table->bigIncrements('id_communication_text');
            $table->string('body_text')->nullable();
            $table->string('phone_client')->nullable();
            $table->string('date_text')->nullable();
            $table->boolean('status_text')->nullable();
            $table->integer('type_direction')->nullable();
            $table->bigInteger('id_room')->unsigned();
            $table->bigInteger('id_account')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communication_text');
    }
}
