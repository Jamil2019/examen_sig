<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCommunicationEmailForeingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('communication_email', function (Blueprint $table) {
            $table->foreign('id_room')->references('id_room')->('room');
            $table->foreign('id_account')->references('id_account')->('account');
           

            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('communication_email', function (Blueprint $table) {
            $table->dropforeign(['id_room']);
            $table->dropforeign(['id_account']);
            //
        });
    }
}
