<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicationEmailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communication_email', function (Blueprint $table) {
            $table->bigIncrements('id_communication_email');
            $table->string('body_email')->nullable();
            $table->string('date_email')->nullable();
            $table->integer('type_direction')->nullable();
            $table->boolean('status_email')->nullable();
            $table->bigInteger('id_room')->unsigned();
            $table->bigInteger('id_account')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communication_email');
    }
}
