<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAccountForeingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account', function (Blueprint $table) {
            $table->foreign('id_time_zone')->references('id_time_zone')->('time_zone');
            $table->foreign('id_account')->references('id_account')->('account');
            $table->foreign('id_state')->references('id_state')->('state');
            $table->foreign('id_contry')->references('id_contry')->('contry');
            $table->foreign('id_type_account')->references('id_type_account')->('type_account');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account', function (Blueprint $table) {
            $table->dropforeign(['id_time_zone']);
            $table->dropforeign(['id_account']);
            $table->dropforeign(['id_state']);
            $table->dropforeign(['id_contry']);
            $table->dropforeign(['id_type_account']);
            //
        });
    }
}
