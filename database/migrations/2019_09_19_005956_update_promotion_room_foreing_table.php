<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePromotionRoomForeingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promotion_room', function (Blueprint $table) {
            $table->foreign('id_promotion')->references('id_promotion')->('promotion');
            $table->foreign('id_room')->references('id_room')->('room');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promotion_room', function (Blueprint $table) {
            $table->dropforeign(['id_promotion']);
            $table->dropforeign(['id_room']);
            //
        });
    }
}
